public class Board{
  private Die die1;
  private Die die2;
  private boolean[] closedTiles;
  public Board(){
    this.die1=new Die();
    this.die2=new Die();
    this.closedTiles=new boolean[12];
  }
  public String toString(){
    String output="";
    for(int i=0;i<this.closedTiles.length;i++){
      if(!closedTiles[i]){
        output=output+(i+1)+", ";
      }
      else{
        output=output+"X, ";
      }
    }
    return output;
  }
  public boolean playATurn(){
    boolean output=false;
    die1.roll();
    die2.roll();
    System.out.println("die 1: "+die1+"\n"+"die 2: "+die2);
    int sum=die1.getPip()+die2.getPip();
    if(this.closedTiles[sum-1]){
      System.out.println("This tile is already shut");
      output=true;
    }
    else{
      closedTiles[sum-1]=true;
      System.out.println("Closing tile: "+sum);
      output=false;
    }
    return output;
  }
}