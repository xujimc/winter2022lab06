import java.util.Scanner;
public class ShutTheBox{
	public static void main(String[] args){
		int player1Wins=0;
		int player2Wins=0;
		Scanner reader = new Scanner(System.in);
		String playAgain="yes";
		Board map = new Board();
		while(playAgain.equals("yes")){
			map=new Board();
			System.out.println("Welcome to Shut The Box!");
			boolean gameOver=false;
			while(!gameOver){
				System.out.println("Player 1's turn");
				System.out.println(map);
				if(map.playATurn()){
					System.out.println("Player 2 wins");
					player2Wins++;
					gameOver=true;
					System.out.println("player 1: "+player1Wins+" wins\nplayer 2: "+player2Wins+" wins\ndo you wish to play again?");
					playAgain=reader.nextLine();
				}
				else{
				System.out.println("Player 2's turn");
				System.out.println(map);
				if(map.playATurn()){
					System.out.println("Player 1 wins");
					player1Wins++;
					gameOver=true;
					System.out.println("player 1: "+player1Wins+" wins\nplayer 2: "+player2Wins+" wins\ndo you wish to play again?");
					playAgain=reader.nextLine();
					}
				}
			}
		}
	}
}