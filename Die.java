import java.util.Random;
public class Die{
	private int pip;
	private Random r;
	public Die(){
		this.pip=1;
		this.r=new Random();
	}
	public int getPip(){
		return this.pip;
	}
	public Random getR(){
		return this.r;
	}
	public void roll(){
		this.pip=r.nextInt(6)+1;
	}
	public String toString(){
		return this.pip+"";
	}
}